﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioAnimal
{
    class Reptil : Animal
    {
        public Reptil(int peso, int altura, int nivelFome, string sons)
            : base(peso, altura, nivelFome, sons)
        {

        }
        public override void Alimentar()
        {
            this.NivelFome = (int)(this.NivelFome * .80);
        }
        public override void Mover()
        {
            this.NivelFome = (int)(this.NivelFome * 1.05);
        }
        public override void Dormir()
        {
            this.NivelFome = (int)(this.NivelFome * .90);
        }
    }
}
