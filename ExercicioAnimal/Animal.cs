﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioAnimal
{
    class Animal
    {
        private int peso;
        private int altura;
        private int nivelFome;
        private string sons;

        public Animal(int peso, int altura, int nivelFome, string sons)
        {
            this.Peso = peso;
            this.Altura = altura;
            this.NivelFome = nivelFome;
            this.Sons = sons;
        }
        public int Peso
        {
            set
            {
                this.peso = value;
            }
            get
            {
                return peso;
            }
        }
        public int Altura
        {
            set
            {
                this.altura = value;
            }
            get
            {
                return altura;
            }
        }
        public int NivelFome
        {
            set
            {
                if(value <= 0 || value >= 100){
                    throw new Exception("Erro, nivel de fome menor que 0 ou maior que 100");
                } else {
                    this.nivelFome = value;
                }
            }
            get
            {
                return nivelFome;
            }
        }
        public string Sons
        {
            set
            {
                this.sons = value;
            }
            get
            {
                return sons;
            }
        }
        public virtual void Alimentar()
        {

        }
        public virtual void Dormir()
        {

        }
        public virtual void Mover()
        {

        }
        public virtual void Falar()
        {

        }
        public virtual void ShowNivelFome()
        {
            Console.WriteLine(this.nivelFome);
        }
    }
}
