﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioAnimal
{
    class Program
    {
        static void Main(string[] args)
        {
            Mamifero humano = new Mamifero(80, 2, 60, "abcdario");
            //humano.Alimentar();
            //humano.Mover();
            humano.Mover();
            humano.ShowNivelFome();
            Console.ReadKey();
        }
    }
}
