﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioAnimal
{
    class Ave : Animal
    {
        public Ave(int peso, int altura, int nivelFome, string sons)
            : base(peso, altura, nivelFome, sons)
        {

        }
        public override void Alimentar()
        {
            this.NivelFome = (int)(this.NivelFome * .70);
        }
        public override void Mover()
        {
            this.NivelFome = (int)(this.NivelFome * 1.02);
        }
        public override void Dormir()
        {
            this.NivelFome = (int)(this.NivelFome * .95);
        }
    }
}
