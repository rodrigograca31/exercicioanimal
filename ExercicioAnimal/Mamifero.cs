﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioAnimal
{
    class Mamifero : Animal
    {
        public Mamifero(int peso, int altura, int nivelFome, string sons)
            : base(peso, altura, nivelFome, sons)
        {

        }
        public override void Alimentar()
        {
            this.NivelFome /= 2;
        }
        public override void Mover()
        {
            this.NivelFome = (int)(this.NivelFome * 1.1);
        }
        public override void Dormir()
        {
            this.NivelFome = (int)(this.NivelFome * .98);
        }
    }

}
